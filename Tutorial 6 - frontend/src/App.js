import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Container } from '@mui/material';
import Boards from './pages/Boards';
import CreateBoard from './pages/CreateBoard';
import Navigation from './components/Navigation';

import NavigationWS from './components/NavigationWS';
import WorkSpace from './pages/WorkSpace';
import CreateWorkSpace from './pages/CreateWorkSpace';
import TasksList from './pages/TasksList';

function App() {
  return (
    <React.Fragment>
      <Container>
        
        <Switch>

          <Route path={['/', '/workspace']} exact>
          <NavigationWS/>
            <WorkSpace />
          </Route>

          <Route path= '/create-workspace' exact>
          <NavigationWS/>  
            <CreateWorkSpace />
          </Route>

          <Route path= '/boards' exact>
          <Navigation/>  
            <Boards />
          </Route>

          <Route path= '/create-board' exact>
          <Navigation/>  
            <CreateBoard />
          </Route>

          <Route path= '/TasksList' exact>
            <TasksList />
          </Route>

        </Switch>

      </Container>
    </React.Fragment>    
  );
};

export default App;
