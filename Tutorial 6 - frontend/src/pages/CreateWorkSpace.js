import React from 'react';
import { useHistory } from 'react-router-dom';
import CreateSpaceForm from '../components/CreateSpaceForm';

function CreateWorkSpace() {

    const history = useHistory();

    function createWorkSpaceHandler(WorkSpace) {
        fetch('http://localhost:9001/workspace', {
            method: 'POST',
            body: JSON.stringify(WorkSpace),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(() => history.replace('/WorkSpace'));
    }

    return (
        <CreateSpaceForm createSpace={createWorkSpaceHandler} />
    );
};

export default CreateWorkSpace;