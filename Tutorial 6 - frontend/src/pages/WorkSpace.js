import React, { useEffect, useState } from 'react';
import ViewWorkSpace from '../components/ViewWorkSpace';

function WorkSpace() {
    const [SpaceData, setSpaceData] = useState([]);

    function getAllSpace() {
        fetch('http://localhost:9001/workspace')
            .then(response => response.json())
            .then(space => {
                setSpaceData(space);
            });
    };

    useEffect(function () {
        getAllSpace();
    }, []);


    return (
        <section>
            <ViewWorkSpace workspaces={SpaceData} />
        </section>
    );
};

export default WorkSpace;