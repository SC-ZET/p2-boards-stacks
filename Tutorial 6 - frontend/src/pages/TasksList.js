import React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { Button, CardActionArea, CardActions, } from '@mui/material';

function TasksList() {
  return (
    <div>
      <Card sx={{ maxWidth: 300 }}>
        <CardActionArea>
          <CardContent>
            <Typography variant="h5">
              To-Do
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button size="small" color="primary">
            + Add a Card
          </Button>
        </CardActions>
      </Card>


      <Card sx={{ maxWidth: 300 }}>
        <CardActionArea>
          <CardContent>
            <Typography variant="h5">
              Doing
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button size="small" color="primary">
            + Add a Card
          </Button>
        </CardActions>
      </Card>

      <Card sx={{ maxWidth: 300 }}>
        <CardActionArea>
          <CardContent>
            <Typography variant="h5">
              Done
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button size="small" color="primary">
            + Add a Card
          </Button>
        </CardActions>
      </Card>

    </div>
  );
}

export default TasksList;