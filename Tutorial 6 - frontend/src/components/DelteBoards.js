import { Button } from '@mui/material';

export default function DeleteBoards(props) {
    
    console.log()
    function deleteBoard() {
        console.log(props.board)
        fetch('http://localhost:9001/board/delete', {
            method: 'DELETE',
            body: JSON.stringify(props.board),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(() => window.location.reload());
    };

    return(
        <div>
            <Button onClick={deleteBoard}>
                Delete
            </Button>
        </div>
    )
}

