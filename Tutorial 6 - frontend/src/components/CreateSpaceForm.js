import { Button, TextField, Typography } from '@mui/material';
import React, { useRef } from 'react';

function CreateSpaceForm(props) {
    const spaceNameRef = useRef();
    const spaceDespRef = useRef();

    function createSpace(e) {
        e.preventDefault();
        const spaceName = spaceNameRef.current.value;
        const spaceDesp = spaceDespRef.current.value;

        const space = {
            name: spaceName,
            description: spaceDesp
        };

        props.createSpace(space);
    };

    return (
        <section style={{ marginTop: '32px' }}>
            <Typography variant='h2' component='h2'>Create New Workspace</Typography>
            <form onSubmit={createSpace}>
                <TextField
                    id='spaceName'
                    placeholder='Workspace Name'
                    variant='outlined'
                    required
                    fullWidth
                    margin='dense'
                    inputRef={spaceNameRef} />
                <TextField
                    id='spaceDesp'
                    placeholder='Workspace Description'
                    variant='outlined'
                    multiline
                    rows={4}
                    required
                    fullWidth
                    margin='dense'
                    inputRef={spaceDespRef} />
                <Button type='submit' variant='contained' color='primary' sx={{ marginTop: '16px' }}>
                    Create Workspace
                </Button>
            </form>
        </section>
    );
};

export default CreateSpaceForm;