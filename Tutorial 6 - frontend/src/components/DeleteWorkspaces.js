import { Button } from '@mui/material';

export default function DeleteWorkspaces(props) {
    
    function deleteWorkspaces() {

        fetch('http://localhost:9001/workspace/delete', {
            method: 'DELETE',
            body: JSON.stringify(props.workspace),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(() => window.location.reload());
    };

    return(
        <div>
            <Button onClick={deleteWorkspaces}>
                Delete
            </Button>
        </div>
    )
}