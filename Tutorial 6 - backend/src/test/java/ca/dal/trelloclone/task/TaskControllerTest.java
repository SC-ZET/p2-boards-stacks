package ca.dal.trelloclone.task;

import ca.dal.trelloclone.task.model.TaskModel;
import io.github.handsomecoder.utils.UUIDUtils;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import java.util.List;
import java.util.stream.Stream;

import static io.github.handsomecoder.utils.StringUtils.valueOf;
import static java.util.Collections.emptyList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = {TaskController.class})
@ExtendWith(SpringExtension.class)
@WebMvcTest(TaskController.class)
@Import(TaskController.class)
@AutoConfigureMockMvc(addFilters = false)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TaskControllerTest {
    private static final String REQUEST_MAPPING = "/task";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TaskService taskService;

    public static Stream<Arguments> testCreateTaskDatasource() {
        return Stream.of(
                Arguments.of(status().isCreated(), UUIDUtils.generate(),
                        "{\n" +
                                "    \"name\": \"My Test Task\",\n" +
                                "    \"description\": \"This is my test task\"\n" +
                                "}"),
                Arguments.of(status().isBadRequest(), UUIDUtils.generate(),
                        "{\n" +
                                "    \"name\": \"My Test Task\",\n" +
                                "}"),
                Arguments.of(status().isBadRequest(), UUIDUtils.generate(), "{\n" +
                        "    \"description\": \"This is my test task\"\n" +
                        "}"),
                Arguments.of(status().isConflict(), null,
                        "{\n" +
                                "    \"name\": \"My Duplicate Task\",\n" +
                                "    \"description\": \"This is my duplicate task\"\n" +
                                "}")
        );
    }

    public static Stream<Arguments> testGetTasksDatasource() {
        return Stream.of(
                Arguments.of(status().isOk(), emptyList())
        );
    }

    public static Stream<Arguments> testAssignTaskDatasource() {
        return Stream.of(
                Arguments.of(status().isOk(), UUIDUtils.generate(), 1L, true),
                Arguments.of(status().isBadRequest(), " ", 1L, true),
                Arguments.of(status().isOk(), UUIDUtils.generate(), 1L, false)
        );
    }

    @ParameterizedTest(name = "{index}: testCreateTask()")
    @MethodSource("testCreateTaskDatasource")
    public void testCreateTask(ResultMatcher expected, String mockServiceResponse, String request) throws Exception {
        when(taskService.createTask(any())).thenReturn(mockServiceResponse);
        mockMvc.perform(post(REQUEST_MAPPING)
                        .contentType(MediaType.APPLICATION_JSON).content(request))
                .andExpect(expected);
    }

    @ParameterizedTest(name = "{index}: testGetTasks()")
    @MethodSource("testGetTasksDatasource")
    public void testGetTasks(ResultMatcher expected, List<TaskModel> mockServiceResponse) throws Exception {
        when(taskService.getTasks()).thenReturn(mockServiceResponse);
        mockMvc.perform(get(REQUEST_MAPPING)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(expected);
    }

    @ParameterizedTest(name = "{index}: testAssignTask()")
    @MethodSource("testAssignTaskDatasource")
    public void testAssignTask(ResultMatcher expected, String taskId, Long userId, boolean mockServiceResponse) throws Exception {
        when(taskService.assignTask(any(), any())).thenReturn(mockServiceResponse);
        mockMvc.perform(put(REQUEST_MAPPING + "/" + taskId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("userId", valueOf(userId)))
                .andExpect(expected);
    }
}