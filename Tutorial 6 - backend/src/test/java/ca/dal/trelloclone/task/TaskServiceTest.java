package ca.dal.trelloclone.task;

import ca.dal.trelloclone.task.model.TaskModel;
import ca.dal.trelloclone.task.model.TaskRequestModel;
import ca.dal.trelloclone.user.model.UserModel;
import ca.dal.trelloclone.user.service.UserService;
import io.github.handsomecoder.utils.UUIDUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.stream.Stream;

import static io.github.handsomecoder.utils.UUIDUtils.generate;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {TaskService.class})
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TaskServiceTest {

    @Autowired
    private TaskService taskService;

    @MockBean
    private TaskRepository taskRepository;

    @MockBean
    private UserService userService;

    private MockedStatic<UUIDUtils> uuidUtils;

    public static Stream<Arguments> testCreateTaskDatasource() {
        return Stream.of(
                Arguments.of("dummy_id", false,
                        TaskModel.create(new TaskRequestModel("My Test Task", "This is my test task")),
                        new TaskRequestModel("My Test Task", "This is my test task")),
                Arguments.of(null, true, null,
                        new TaskRequestModel("My Test Task", "This is my test task"))
        );
    }

    public static Stream<Arguments> testGetTaskDatasource() {
        return Stream.of(
                Arguments.of(emptyList(), emptyList()),
                Arguments.of(singletonList(new TaskModel("a", "b")),
                        singletonList(new TaskModel("a", "b")))
        );
    }

    public static Stream<Arguments> testAssignTaskDatasource() {
        return Stream.of(
                Arguments.of(true, generate(), 1L, new UserModel(), 1),
                Arguments.of(false, generate(), 1L, null, 1),
                Arguments.of(false, generate(), 1L, new UserModel(), 0)
        );
    }

    @BeforeAll
    void setUpForTestSuite() {
        uuidUtils = Mockito.mockStatic(UUIDUtils.class);
        uuidUtils.when(UUIDUtils::generate).thenReturn("dummy_id");
    }

    @AfterAll
    void tearDownTestSuite() {
        uuidUtils.close();
    }

    @ParameterizedTest(name = "{index}: testCreateTask()")
    @MethodSource("testCreateTaskDatasource")
    public void testCreateTask(String expected, boolean isExists, TaskModel savedEntity, TaskRequestModel request) {
        when(taskRepository.save(any())).thenReturn(savedEntity);
        when(taskRepository.existsByName(any())).thenReturn(isExists);

        assertEquals(expected, taskService.createTask(request));
    }

    @ParameterizedTest(name = "{index}: testGetTasks()")
    @MethodSource("testGetTaskDatasource")
    public void testGetTasks(List<TaskModel> expected, Iterable<TaskModel> mockedResponse) {
        when(taskRepository.findAll()).thenReturn(mockedResponse);
        assertArrayEquals(expected.toArray(), taskService.getTasks().toArray());
    }

    @ParameterizedTest(name = "{index}: testAssignTask()")
    @MethodSource("testAssignTaskDatasource")
    public void testAssignTask(boolean expected, String taskId, Long userId,
                               UserModel mockedUser, int mockedResponse) {

        when(userService.getUserById(any())).thenReturn(mockedUser);
        when(taskRepository.assignTask(any(), any())).thenReturn(mockedResponse);

        assertEquals(expected, taskService.assignTask(taskId, userId));
    }
}