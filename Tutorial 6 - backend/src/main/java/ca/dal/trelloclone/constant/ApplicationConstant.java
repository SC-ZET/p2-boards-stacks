package ca.dal.trelloclone.constant;

public class ApplicationConstant {

    public static final String ID = "id";
    public static final String STATUS = "status";
    public static final String MESSAGE = "message";
    
}
