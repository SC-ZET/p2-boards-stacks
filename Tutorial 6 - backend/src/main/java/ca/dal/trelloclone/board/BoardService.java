package ca.dal.trelloclone.board;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BoardService {

    @Autowired
    private BoardRepository boardRepository;

    public Integer createBoard(BoardModel board) {
        return boardRepository.save(board).getId();
    }

    public void deleteBoard(BoardModel board) {
        boardRepository.delete(board);
    }

    public List<BoardModel> getBoards() {
        return boardRepository.findAll();
    }
}
