package ca.dal.trelloclone.task;

import ca.dal.trelloclone.task.model.TaskModel;
import ca.dal.trelloclone.task.model.TaskRequestModel;
import ca.dal.trelloclone.user.model.UserModel;
import ca.dal.trelloclone.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static io.github.handsomecoder.utils.ObjectUtils.isNull;

@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserService userService;

    public String createTask(TaskRequestModel request) {
        if (taskRepository.existsByName(request.getName())) {
            return null;
        }

        return taskRepository.save(TaskModel.create(request)).getId();
    }

    public List<TaskModel> getTasks() {
        return Streamable.of(taskRepository.findAll()).toList();
    }

    @Transactional
    public boolean assignTask(String taskId, Long userId) {

        UserModel user = userService.getUserById(userId);

        if (isNull(user)) {
            return false;
        }

        return taskRepository.assignTask(taskId, user) == 1;
    }
}
