package ca.dal.trelloclone.task;

import ca.dal.trelloclone.task.model.TaskModel;
import ca.dal.trelloclone.user.model.UserModel;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends CrudRepository<TaskModel, String> {

    @Modifying
    @Query("UPDATE tasks set assignee = :user where id = :taskId")
    int assignTask(String taskId, UserModel user);
    
    boolean existsByName(String name);
}
