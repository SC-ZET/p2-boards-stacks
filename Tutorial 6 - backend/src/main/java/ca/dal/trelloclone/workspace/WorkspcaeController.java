package ca.dal.trelloclone.workspace;

import ca.dal.trelloclone.board.BoardModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static ca.dal.trelloclone.constant.ApplicationConstant.ID;
import static ca.dal.trelloclone.constant.ApplicationConstant.STATUS;
import static io.github.handsomecoder.utils.ObjectUtils.isNull;
import static java.util.Collections.singletonMap;
import static org.springframework.http.ResponseEntity.status;


@RestController
@RequestMapping("workspace")
@CrossOrigin("*")
public class WorkspcaeController {

    @Autowired
    private WorkspaceService workspaceService;

    @PostMapping("")
    public ResponseEntity<Map<String, Integer>> createWorkspace(@RequestBody WorkspaceModel workspace) {
        Integer id = workspaceService.createWorkspace(workspace);
        HttpStatus status = isNull(id) ? HttpStatus.CONFLICT : HttpStatus.CREATED;
        return status(status).body(singletonMap(ID, id));
    }

    @PutMapping("")
    public ResponseEntity<Map<String, Integer>> updateWorkspace(@RequestBody WorkspaceModel workspace) {
        Integer id = workspaceService.createWorkspace(workspace);
        HttpStatus status = isNull(id) ? HttpStatus.CONFLICT : HttpStatus.CREATED;
        return status(status).body(singletonMap(ID, id));
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Map<String, Integer>> deleteBoard(@RequestBody WorkspaceModel workspace) {
        try{
            workspaceService.deleteWorkspace(workspace);
        }
        catch(Exception e){
            status(HttpStatus.CONFLICT).body(singletonMap(STATUS, 0));
        }
        return status(HttpStatus.OK).body(singletonMap(STATUS, 1));
    }

    @GetMapping("")
    public List<WorkspaceModel> getWorkspaces() {
        return workspaceService.getWorkspaces();
    }

}

