package ca.dal.trelloclone.workspace;

import ca.dal.trelloclone.board.BoardModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WorkspaceService {

    @Autowired
    private WorkspaceRepository workspaceRepository;

    public Integer createWorkspace(WorkspaceModel workspace) {
        return workspaceRepository.save(workspace).getId();
    }

    public void deleteWorkspace(WorkspaceModel workspace) {
        workspaceRepository.delete(workspace);
    }

    public List<WorkspaceModel> getWorkspaces() {
        return workspaceRepository.findAll();
    }
}
