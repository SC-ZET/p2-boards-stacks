package ca.dal.trelloclone.user.service;

import ca.dal.trelloclone.user.model.UserModel;
import ca.dal.trelloclone.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface UserService
{
    public UserModel addUser(UserModel userModel);
    public UserModel getUserById(Long userId);
    public List<UserModel> getAllUsers();
}
